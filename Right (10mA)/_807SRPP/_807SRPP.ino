/*
807SRPP V3.3 Right (10mA instrument)
 Date 24.04.2014
 Version Ampli 3.3.0
 Version Prog 1.2.1
 *******************
 24.04.2014: Added temperature check
 */
#include <SimpleTimer.h>
#include <EasyTransfer.h>
#include <AmpTransfer.h>
#include <SmoothAnalogInput.h>
#include <Blink.h>
#include <PID.h>
#include <OneWire.h>
#include <DallasTemperature.h>

static const byte SRPP807R_ID = 209;
static const byte ampId = SRPP807R_ID;

// Pin Config
#define ledOnBoard		13
#define regulatorPinHi		3
#define regulatorPinLow		5
#define oneWireBus              6
#define ledPin		        7
#define indicatorPin            9
#define htRelayPin		10
#define outRelayPin             11
#define commonRelayPin          12
#define voltageOutPin	        A5    
#define voltageRefPin 	        A4    
#define voltageScreenPin	A2    

// Constants
#define regulatingTreshold           2          // Max 512
#define stabilizationTreshold        2          // Max 512
#define maxTreshold                  480        // Max 512
#define heatMaxTime                  40         // seconds. Heat time 
#define highVoltageMaxTime           2          // seconds. Time starting the high voltage before the regulation.          
#define stabilizationMaxTime         60         // seconds. Time required to stabilize the current
#define stabilizedTime               50         // milli seconds. Time required to leave the stabilization sequence when stabilized
#define errorMaxTime                 1000       // Milli-seconds
#define currentRatioIndicator	     0.25
#define startingP                    8          // Stable until 25
#define startingI                    0.003        
#define startingD                    0.00006    
#define stabilizedP                  50           
#define stabilizedI                  0.02              
#define stabilizedD                  0
#define pidOutputDefault             32767
#define indicatorCenter              133
#define indicatorMax                 255  
#define stabilizationErrorCountMax   3
#define screenVoltageMin             200
#define targetVoltage                512
#define differentialErrorCountMax    5000
#define airTempMax                   80
#define shuntTempMax                 90
#define regTempMax                   90
#define transfoTempMax               110

// Internal use
SimpleTimer sendTimer;
SimpleTimer tempMeasureTimer;
SmoothAnalogInput refInput;
SmoothAnalogInput outInput;
SmoothAnalogInput screenInput;
Blink ledBlink;
OneWire oneWire(oneWireBus);
DallasTemperature tempSensors(&oneWire);
unsigned int stepMaxTime = 0;
unsigned int stepElapsedTime = 0;
unsigned int stepMaxValue = 0;
unsigned int stepCurValue = 0;
unsigned int heatTime = 0;
unsigned long highVoltageStartTime = 0;
unsigned int highVoltageTime = 0;
unsigned int regulatingTime = 0;
unsigned long stabilizationStartTime = 0;
unsigned long stabilizedStartTime = 0;
unsigned int stabilizationTime = 0;
unsigned long errorTime = 0;
unsigned long screenErrorTime = 0;
double refAverage;
double outAverage;
double screenAverage = 1024;
double pidOutput = pidOutputDefault;
double pidSetPoint = 0;
byte airTemp = 0;
byte shuntTemp = 0;
byte regTemp = 0;
byte transfoTemp = 0;
unsigned int differentialErrorCount = 0;

// Init regulators
PID regulator(&outAverage, &pidOutput, &pidSetPoint, startingP, startingI, startingD, 0, 65535, 100, true);

// Errors
#define NO_ERR                  0      // No error
#define ERR_SCREENCURRENT       2      // Screen Current Error
#define ERR_STABILIZINGTOOLONG  3      // Stabilization too long
#define ERR_OUTOFRANGE          4      // Out of range during normal function
#define ERR_AIRTEMPTOOHIGH      5      // Air inside amplifier temperature too high 
#define ERR_SHUNTTEMPTOOHIGH    6      // Shunt regulator temperature too high
#define ERR_REGTEMPTOOHIGH      7      // Driver regulator temperature too high
#define ERR_TRANSFOTEMPTOOHIGH  8      // Internal transformer temperature too high
int errorNumber = NO_ERR;
int blinkErrorCount = 0;

// Sequence:
#define SEQ_HEAT         0  // 0: Heat tempo 
#define SEQ_STARTING     1  // 1: Starting High Voltage
#define SEQ_REGULATING   2  // 2: Waiting for reg
#define SEQ_FUNCTION     3  // 3: Normal Fonction
#define SEQ_FAIL         4  // 4: Fail
int sequence = SEQ_HEAT;

// Temp indexes in one wire bus
#define AIR_TEMPERATURE      2
#define SHUNT_TEMPERATURE    1
#define REG_TEMPERATURE      0
#define TRANSFO_TEMPERATURE  3

// Diagnostic
EasyTransfer dataTx; 
dataResponse dataTxStruct;

void sendDatas()
{    
  // Send datas
  dataTxStruct.message = MESSAGE_SENDVALUES;
  dataTxStruct.step = sequence;
  dataTxStruct.stepMaxTime = stepMaxTime;
  dataTxStruct.stepElapsedTime = stepElapsedTime;
  dataTxStruct.stepMaxValue = stepMaxValue;
  dataTxStruct.stepCurValue = stepCurValue;
  dataTxStruct.tickCount = millis();
  dataTxStruct.measure0 = map(refAverage, 0, 1023, 0, 255); // Input 1024 max, but I transfer only a range of 255
  dataTxStruct.measure1 = map(outAverage, 0, 1023, 0, 255);
  dataTxStruct.output0 = map(pidOutput, 0, 65535, 0, 255);    
  dataTxStruct.temperature0 = airTemp; 
  dataTxStruct.temperature1 = shuntTemp; 
  dataTxStruct.temperature2 = regTemp; 
  dataTxStruct.temperature3 = transfoTemp; 
  dataTxStruct.minValue = (targetVoltage - maxTreshold) >> 2;
  dataTxStruct.refValue = targetVoltage >> 2;
  dataTxStruct.maxValue = (targetVoltage + maxTreshold) >> 2;
  dataTxStruct.errorNumber = errorNumber;
  dataTx.sendData();
}

void measureTemp()
{
  // Send the command to get temperatures
  tempSensors.requestTemperatures(); 
  airTemp = constrain(tempSensors.getTempCByIndex(AIR_TEMPERATURE), 0, 255);
  shuntTemp = constrain(tempSensors.getTempCByIndex(SHUNT_TEMPERATURE), 0, 255);
  regTemp = constrain(tempSensors.getTempCByIndex(REG_TEMPERATURE), 0, 255);
  transfoTemp = constrain(tempSensors.getTempCByIndex(TRANSFO_TEMPERATURE), 0, 255);
}

void HTRelayOn() 
{
  digitalWrite(htRelayPin, LOW); 
}

void HTRelayOff() 
{
  digitalWrite(htRelayPin, HIGH); 
}

void OutputRelayOn() 
{
  digitalWrite(outRelayPin, LOW); 
}

void OutputRelayOff() 
{
  digitalWrite(outRelayPin, HIGH); 
}

void SetRegulatorOutput(long value)
{
  int hword = value >> 0x8;
  int lword = value & 0xFF;

  analogWrite(regulatorPinLow, lword);
  analogWrite(regulatorPinHi, hword);
}

void Reset()
{
  regulator.SetEnabled(false);  
  pidOutput = pidOutputDefault;
  SetRegulatorOutput(pidOutput);
  digitalWrite(commonRelayPin, HIGH); 
  digitalWrite(htRelayPin, HIGH); 
  digitalWrite(outRelayPin, HIGH); 
}

void InitRegulator()
{
  pidSetPoint = targetVoltage;
  pidOutput = pidOutputDefault;
  SetRegulatorOutput(pidOutput);
  regulator.SetEnabled(true);
}

void Regulate(void)
{
  if (sequence == SEQ_FUNCTION)
  {
    regulator.SetGains(stabilizedP, stabilizedI, stabilizedD);
  }
  else
  {
    regulator.SetGains(startingP, startingI, startingD);
  }

  if (regulator.Compute()) {
    SetRegulatorOutput(pidOutput);
  }
}

boolean CheckInRange(unsigned int treshold)
{
  return outAverage > (targetVoltage - treshold) && outAverage < (targetVoltage + treshold) && refAverage > (512 - maxTreshold) && refAverage < (512 + maxTreshold);
}

unsigned int calcRegulationProgress(unsigned int treshold)
{
  unsigned int percentProgress = 100;
  unsigned int minValue = targetVoltage - treshold;
  unsigned int maxValue = targetVoltage + treshold;

  if (outAverage < minValue)
  {
    percentProgress = 100 * outAverage / minValue;
  }
  else if (outAverage > maxValue)
  {
    percentProgress = 100 * (1 - (outAverage - maxValue) / (1023 - maxValue));
  }

  return constrain(percentProgress, 0, 100);
}

// the setup routine runs once when you press reset:
void setup() 
{                
  // initialize the digital pin as an output.
  pinMode(ledOnBoard, OUTPUT);     
  pinMode(htRelayPin, OUTPUT);     
  pinMode(outRelayPin, OUTPUT);     
  pinMode(commonRelayPin, OUTPUT);     
  pinMode(ledPin, OUTPUT);  

  refInput.attach(voltageRefPin, 32);
  outInput.attach(voltageOutPin, 32);
  screenInput.attach(voltageScreenPin, 32);

  ledBlink.Setup(ledPin, false);
  sendTimer.setInterval(200, sendDatas);

  tempSensors.begin();   
  tempMeasureTimer.setInterval(30000, measureTemp);
  tempSensors.requestTemperatures(); 
  measureTemp();
  
  Serial.begin(9600);
  dataTxStruct.id = ampId;
  dataTx.begin(details(dataTxStruct), &Serial);
}

// the loop routine runs over and over again forever:
void loop() 
{ 
  unsigned long currentTime = millis();
  
  refAverage = refInput.read();
  outAverage = outInput.read();

  if (sequence >= SEQ_STARTING)
  {
    screenAverage = screenInput.read();
  }

  // Diagnostic
  sendTimer.run();

  if (sequence >= SEQ_STARTING)
  {
    int indicatorValue = sequence == SEQ_FUNCTION ? refAverage : outAverage;
    indicatorValue = constrain(indicatorCenter - (512 - indicatorValue) * currentRatioIndicator, 0, indicatorMax);
    analogWrite(indicatorPin, indicatorValue); 
  }
  else
  {
    analogWrite(indicatorPin, indicatorCenter); 
  }

  if (screenAverage < screenVoltageMin)
  {
    if (screenErrorTime == 0){
      screenErrorTime = currentTime;
    }

    if (currentTime - screenErrorTime > errorMaxTime)
    {
    // Fail: Screen current too high
    sequence = SEQ_FAIL;
    errorNumber = ERR_SCREENCURRENT;
  }
  } 
  else {
    screenErrorTime = 0;
  }

  if (sequence != SEQ_FAIL){
    tempMeasureTimer.run();

    if (airTemp > airTempMax) {
      // Fail, air temperature too high
      sequence = SEQ_FAIL;
      errorNumber = ERR_AIRTEMPTOOHIGH;
    }
    else if (shuntTemp > shuntTempMax) {
      // Fail, regulators shunt temperature too high
      sequence = SEQ_FAIL;
      errorNumber = ERR_SHUNTTEMPTOOHIGH;
    }
    else if (regTemp > regTempMax) {
      // Fail, regulators temperature too high
      sequence = SEQ_FAIL;
      errorNumber = ERR_REGTEMPTOOHIGH;
    }
    else if (transfoTemp > transfoTempMax) {
      // Fail, internal transformer temperature too high
      sequence = SEQ_FAIL;
      errorNumber = ERR_TRANSFOTEMPTOOHIGH;
    }
  }

  switch (sequence)
  {  
  case SEQ_HEAT: 
    Reset();
    ledBlink.Execute(400, 400);

    heatTime = currentTime / 1000;

    // Diagnostic
    stepMaxTime = heatMaxTime;
    stepElapsedTime = heatTime;
    stepMaxValue = heatMaxTime;
    stepCurValue = heatTime;

    if(heatTime < heatMaxTime)
    {
      break;
    }

    // Diagnostic, force 100%
    stepMaxTime = heatMaxTime;
    stepElapsedTime = heatMaxTime;
    stepMaxValue = heatMaxTime;
    stepCurValue = heatMaxTime;

    sequence++;
    ledBlink.On();
    delay(500);  
    highVoltageStartTime = currentTime;

  case SEQ_STARTING:
    // Starting High Voltage
    HTRelayOn();    
    ledBlink.On();

    highVoltageTime = (currentTime - highVoltageStartTime) / 1000;

    // Diagnostic
    stepMaxTime = highVoltageMaxTime;
    stepElapsedTime = highVoltageTime;
    stepMaxValue = highVoltageMaxTime;
    stepCurValue = highVoltageTime;

    if(highVoltageTime < highVoltageMaxTime)
    {
      break;  
    }  

    stabilizationStartTime = currentTime;
    stabilizedStartTime = 0;
    InitRegulator();
    sequence++;

  case SEQ_REGULATING: 
    // Waiting for reg    
    Regulate(); 

    ledBlink.Execute(20, 500);

    stabilizationTime = (currentTime - stabilizationStartTime) / 1000;
    if(stabilizationTime > stabilizationMaxTime)
    {
      // Fail, too late
      sequence = SEQ_FAIL;
      errorNumber = ERR_STABILIZINGTOOLONG;
      break;
    }

    // Diagnostic
    stepMaxTime = stabilizationMaxTime;
    stepElapsedTime = stabilizationTime;
    stepMaxValue = 100;
    stepCurValue = calcRegulationProgress(regulatingTreshold); 

    if (!CheckInRange(regulatingTreshold))
    {
      stabilizedStartTime = 0;
      break;
    }
    
    if (stabilizedStartTime == 0){
      stabilizedStartTime = currentTime;
      break;
    }
    
    if(currentTime - stabilizedStartTime < stabilizedTime)
    {
      break;
    }

    sequence++;

  case SEQ_FUNCTION:
    // Normal Fonction, wait and see        
    ledBlink.Off();
    HTRelayOn();
    OutputRelayOn();
    Regulate(); 
    
    if (!CheckInRange(maxTreshold))
    {
      differentialErrorCount++;
    }
    else
    {
      differentialErrorCount = 0;
    }

    if (differentialErrorCount > differentialErrorCountMax)
    {
      // Fail voltage error
      if (errorTime == 0){
        errorTime = currentTime;
      }

      if (currentTime - errorTime > errorMaxTime)
      {
        // Fail current error
      sequence = SEQ_FAIL;
      errorNumber = ERR_OUTOFRANGE;
      break;      
    }
    } 
    else {
      errorTime = 0;
    }

    // Diagnostic
    stepMaxTime = 0;
    stepElapsedTime = 0;
    stepMaxValue = 0;
    stepCurValue = 0;

    break;

  default: 
    // Fail, protect mode
    Reset();
    ledBlink.Execute(250, errorNumber, 1200);

    // Diagnostic
    stepMaxTime = 0;
    stepElapsedTime = 0;
    stepMaxValue = 0;
    stepCurValue = 0;
  }  
}

