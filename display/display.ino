/* The circuit:
 * SD card attached to SPI bus as follows:
 ** MOSI - pin 51
 ** MISO - pin 50
 ** CLK - pin 52
 ** CS - pin 53
 */
#include <EasyTransfer.h>
#include <AmpTransfer.h>
#include <UTFT.h>

unsigned int SRPPV3L_ID = 208;
unsigned int SRPPV3R_ID = 209;

// Declare which fonts we will be using
extern uint8_t SmallFont[];
extern uint8_t BigFont[];
extern uint8_t SevenSegNumFont[];

// Set the pins to the correct ones for your development shield
// ------------------------------------------------------------
// Arduino Uno / 2009:
// -------------------
// Standard Arduino Uno/2009 shield            : <display model>,A5,A4,A3,A2
// DisplayModule Arduino Uno TFT shield        : <display model>,A5,A4,A3,A2
//
// Arduino Mega:
// -------------------
// Standard Arduino Mega/Due shield            : <display model>,38,39,40,41
// CTE TFT LCD/SD Shield for Arduino Mega      : <display model>,38,39,40,41
//
// Remember to change the model parameter to suit your display module!
UTFT myGLCD(ITDB32S,38,39,40,41);
unsigned int const X_MAX = 26;
unsigned int const Y_MAX = 14;

char buffer[X_MAX * Y_MAX];
char tmpBuffer[X_MAX * Y_MAX];
int currentLine = 0;

static const boolean debug = true;

EasyTransfer readDatas; 
dataResponse readDatasStruct;

EasyTransfer sendRequest;  
ampRequest sendRequestStruct;

ampRequestInfos ampInfo;

// LED vars 
const int ledPin = 13;

// On the Ethernet Shield, CS is pin 4. Note that even if it's not
// used as the CS pin, the hardware CS pin (10 on most Arduino boards,
// 53 on the Mega) must be left as an output or the SD library
// functions will not work.
const int chipSelect = 53;

unsigned long stabilizedTime = 0;
unsigned long failTime = 0;
unsigned long lastReceivedTime = 0;
unsigned int lastStep = 0;

unsigned int const MAX_BUFFER_SIZE = 2048;
char json[MAX_BUFFER_SIZE];

void setup(){
  Serial.begin(9600);  
  Serial3.begin(9600);  

  // make sure that the default chip select pin is set to
  // output, even if you don't use it:
  pinMode(chipSelect, OUTPUT);

  pinMode(ledPin, OUTPUT);

  pinMode(12, OUTPUT);
  digitalWrite(12, HIGH);

  readDatas.begin(details(readDatasStruct), &Serial3);

  sendRequest.begin(details(sendRequestStruct), &Serial);

  ampInfo.id = 0;
  ampInfo.lastLogTime = 0;
  ampInfo.lastReceivedTime = 0;
  ampInfo.canBeTransfered = 0;

  myGLCD.InitLCD();

  myGLCD.clrScr();
  myGLCD.setFont(BigFont);
  myGLCD.setColor(0, 255, 0);
  myGLCD.setBackColor(0, 0, 0);
}

void processDatas(dataResponse datas, ampRequestInfos* ampInfo)
{
  if (datas.message == MESSAGE_SENDVALUES)
  {
    if (debug)
    {
      Serial.println("Datas revceived for " + String(datas.id));
    }

    if (datas.id == SRPPV3L_ID || datas.id == SRPPV3R_ID){
      printSRPPV3(datas);
    } 
    else {
      printJSON(datas);
    } 

    // If the amp is stabilized from more than 30 seconds, transfer datas only all 5 seconds to prevent big logs
    unsigned long time = millis();
    unsigned long logDelay = 0;
    if (datas.stepMaxTime == 0 && datas.errorNumber == 0)
    {
      // Stabilized
      if (stabilizedTime == 0)
      {
        stabilizedTime = time;
      }
      else if (time - stabilizedTime > 30000)
      {
        logDelay = 5000;
      }
    }
    else
    {
      stabilizedTime = 0;
    }

    // If tha amp has fail from more than 30 seconds, transfer datas only all 5 seconds to prevent big logs
    if (datas.errorNumber > 0)
    {
      // Stabilized
      if (failTime == 0)
      {
        failTime = time;
      }
      else if (time - failTime > 30000)
      {
        logDelay = 5000;
      }
    }
    else
    {
      failTime = 0;
    }    

    ampInfo->id = datas.id;
    ampInfo->lastReceivedTime = time; 
    ampInfo->canBeTransfered = 1;

    lastReceivedTime = millis();
  }
  else if (debug)
  {
    Serial.println("Unknown message received from " + String(datas.id) + " number: " + datas.message);
  }
}

void sendReset()
{
  sendRequestStruct.message = MESSAGE_RESET;
  sendRequestStruct.id = MESSAGE_ID_ALL;
  sendRequest.sendData();  
}

void loop(){

}

/*
  SerialEvent occurs whenever a new data comes in the
 hardware serial RX.  This routine is run between each
 time loop() runs, so using delay inside loop can delay
 response.  Multiple bytes of data may be available.
 */
void serialEvent3() {
  if(readDatas.receiveData()){
    processDatas(readDatasStruct, &ampInfo);
    digitalWrite(ledPin, HIGH);
    delay(1);
    digitalWrite(ledPin, LOW);
  }
}

void printJSON (dataResponse datas){
  myGLCD.print("Unknown amplifier id: " + String(datas.id), 2, 2 ,0);
}

void printSRPPV3 (dataResponse datas){
  if (datas.step != lastStep){
    myGLCD.clrScr();
    lastStep = datas.step;
  }

  if (datas.step == 0){
    myGLCD.print("Heating: " + String(datas.stepElapsedTime) + "s/" + String(datas.stepMaxTime) + "s", 2, 2 ,0);
  }
  else if (datas.step == 1){
    myGLCD.print("Starting: " + String(datas.stepElapsedTime) + "s/" + String(datas.stepMaxTime) + "s", 2, 2 ,0);
  }
  else if (datas.step == 2){
    myGLCD.print("Stabilizing: " + String(datas.stepCurValue * 100 / datas.stepMaxValue) + "%", 2, 2 ,0);
  }
  else if (datas.step == 3){
    unsigned int percentCommand = datas.measure0 * 100 / 255;
    unsigned int percentOut = datas.measure1 * 100 / 255;

    unsigned int output1 = datas.output0 * 100 / 255;

    unsigned int percentAirTemp = max(min(datas.temperature0 - 25, 100), 0.1);
    unsigned int percentShuntTemp = max(min(datas.temperature1 - 25, 100), 0.1);
    unsigned int percentRegTemp = max(min(datas.temperature2  - 25, 100), 0.1);
    unsigned int percentTransfoTemp = max(min(datas.temperature3  - 25, 100), 0.1);

    myGLCD.print("Measure:", 2, 2 ,0);
    myGLCD.print("Command:" + String(round((percentCommand * 2 - 99.6)) * 41.8) + "mV", 2, 22 ,0);
    myGLCD.print("Output:" + String(round((percentOut * 2 - 99.6)) * 41.8) + "mV", 2, 42 ,0);

    myGLCD.print("Temperatures:", 2, 82 ,0);
    myGLCD.print("Air:     " + String(percentAirTemp + 25) + " degC", 2, 102 ,0);
    myGLCD.print("Shunt:   " + String(percentShuntTemp + 25) + " degC", 2, 122 ,0);
    myGLCD.print("Reg:     " + String(percentRegTemp + 25) + " degC", 2, 142 ,0);
    myGLCD.print("Transfo: " + String(percentTransfoTemp + 25) + " degC", 2, 162 ,0);
  }
  else {
    myGLCD.print("Error:", 2, 2 ,0);
    if (datas.errorNumber == 2){
      myGLCD.print("Screen current", 2, 22 ,0);
    }
    else if (datas.errorNumber == 3){
      myGLCD.print("Stabilization too long", 2, 22 ,0);
    }
    else if (datas.errorNumber == 4){
      myGLCD.print("Out of range", 2, 22 ,0);
    }
    else if (datas.errorNumber == 5){
      myGLCD.print("Air inside amplifier temperature too high", 2, 22 ,0);
    }
    else if (datas.errorNumber == 6){
      myGLCD.print("Shunt regulator temperature too high", 2, 22 ,0);
    }
    else if (datas.errorNumber == 7){
      myGLCD.print("Driver regulator temperature too high", 2, 22 ,0);
    }
    else if (datas.errorNumber == 8){
      myGLCD.print("Internal transformer temperature too high", 2, 22 ,0);
    }
  }
}




































































































































